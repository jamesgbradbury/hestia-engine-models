const { existsSync } = require('fs');
const { join } = require('path');
const csv = require('csvtojson/v2');

const {
  MODELS_PATH, SRC_DIR,
  readModelContent, writeJSON, unique, isInit, parseJSONData,
  cleanModel, findModelKey, findTerms, listModels, parseFileModel,
  lookupToJson
} = require('./utils');

const readCsv = async file => await csv().fromFile(file);

const processModel = (filepath) => {
  const { model, runName } = parseFileModel(filepath);
  const path = join(MODELS_PATH, filepath);
  const docPath = path.replace('.py', '.md');
  const content = readModelContent(filepath);
  const modelKey = findModelKey(content);
  const terms = findTerms(content);
  const returns = parseJSONData(content, 'RETURNS');
  return [
    ...terms.flatMap(term => [
      {
        path,
        docPath,
        model: cleanModel(model),
        term,
        methodTier: returns?.Emission?.[0]?.methodTier
      }
    ]).filter(Boolean),
    ...(
      modelKey && runName ? [{
        path,
        docPath,
        model: cleanModel(model),
        modelKey: runName
      }] : []
    )
  ];
};

const processFolder = model => {
  const path = join(MODELS_PATH, model);
  const docPath = join(path, 'README.md');
  return existsSync(docPath) ? [{
    path,
    docPath,
    model: cleanModel(model)
  }] : [];
};

const processFile = filepath => {
  const paths = filepath.split('/');
  const file = paths.pop();
  const folder = paths.join('/');
  return isInit(file) ? processFolder(folder) : processModel(filepath);
};

const processEcoInventTerms = async () => {
  const model = 'ecoinventV3';
  const path = join(MODELS_PATH, 'cycle', 'input', model);
  const dataPath = join(SRC_DIR, 'data', model, model + '_excerpt.csv');
  const data = existsSync(dataPath) ? await readCsv(dataPath) : [];
  const terms = unique(data.flatMap(({ emissionsResourceUse }) => emissionsResourceUse.map(({ term }) => term['@id'])));
  return terms.map(term => ({
    path: path + '.py',
    docPath: path + '.md',
    model,
    term
  }));
};

const processTermsFromLookup = async (model, key) => {
  const path = join(MODELS_PATH, model, key);
  const content = readModelContent(`${model}/${key}.py`);
  const lookups = parseJSONData(content, 'LOOKUPS');

  const terms = (await Promise.all(
    Object.entries(lookups).map(async ([name, col]) => ({
      data: await lookupToJson(`${name}.csv`),
      col
    }))
  ))
    .flatMap(({ data, col }) => data.map(v => v[col]))
    .filter(Boolean)
    .flatMap(v => v.split(';'))
    .filter(Boolean)
    .flatMap(v => v.split(':')[0])
    .filter(Boolean);

  return unique(terms).map(term => ({
    path: path + '.py',
    docPath: path + '.md',
    model,
    term
  }));
};

const processAdditionalLinks = async () => (await Promise.all([
  processTermsFromLookup('agribalyse2016', 'fuelElectricity')
])).flat();

const run = async () => {
  const ecoinventLinks = await processEcoInventTerms();
  const additionalLinks = await processAdditionalLinks();
  const files = listModels();
  const links = files.flatMap(processFile);
  writeJSON('model-links.json', { links: [...links, ...additionalLinks], ecoinventLinks });
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
