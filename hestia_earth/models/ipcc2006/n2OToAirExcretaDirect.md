## N2O, to air, excreta, direct

Nitrous oxide emissions to air, from animal excreta.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [n2OToAirExcretaDirect](https://hestia.earth/term/n2OToAirExcretaDirect)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2006](https://hestia.earth/term/ipcc2006)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean`
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta) and optional:
      - a list of [properties](https://hestia.earth/schema/Input#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
    - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta)
  - Data completeness assessment for excreta: [completeness.excreta](https://hestia.earth/schema/Completeness#excreta) must be `True`

This model works on the following Node type with identical requirements:

* [Cycle](https://hestia.earth/schema/Cycle)
* [Transformation](https://hestia.earth/schema/Transformation)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `inputTermTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2006 import run

print(run('n2OToAirExcretaDirect', Cycle))
```
