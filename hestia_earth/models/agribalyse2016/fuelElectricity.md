## Fuel and Electricity

This model calculates fuel and electricity data from the number of hours each machine is operated for using.

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - [methodModel](https://hestia.earth/schema/Input#methodModel) with [agribalyse2016](https://hestia.earth/term/agribalyse2016)
  - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [fuel](https://hestia.earth/glossary?termType=fuel)
  - [value](https://hestia.earth/schema/Input#value)
  - [operation](https://hestia.earth/schema/Input#operation)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - Data completeness assessment for electricityFuel: [completeness.electricityFuel](https://hestia.earth/schema/Completeness#electricityFuel) must be `False`
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [operation](https://hestia.earth/glossary?termType=operation) and [value](https://hestia.earth/schema/Practice#value) `> 0`

### Lookup used

- [operation.csv](https://hestia.earth/glossary/lookups/operation.csv) -> `fuelUse`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.agribalyse2016 import run

print(run('fuelElectricity', Cycle))
```
