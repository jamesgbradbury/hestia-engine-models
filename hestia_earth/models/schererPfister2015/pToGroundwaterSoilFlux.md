## P, to groundwater, soil flux

The total amount of phosphorus which leaches from the soil into the groundwater, including from phosphorus added in fertiliser, excreta, and residue, and from background leaching due to natural processes.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [pToGroundwaterSoilFlux](https://hestia.earth/term/pToGroundwaterSoilFlux)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [schererPfister2015](https://hestia.earth/term/schererPfister2015)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` and a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region) and a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [drainageClass](https://hestia.earth/term/drainageClass)
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [organicFertiliser](https://hestia.earth/glossary?termType=organicFertiliser)

### Lookup used

- [organicFertiliser.csv](https://hestia.earth/glossary/lookups/organicFertiliser.csv) -> `OrganicFertiliserClassification`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.schererPfister2015 import run

print(run('pToGroundwaterSoilFlux', Cycle))
```
