## Below ground crop residue

The total amount of below ground crop residue as dry matter. Properties can be added, such as the nitrogen composition.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [term](https://hestia.earth/schema/Product#term) with [belowGroundCropResidue](https://hestia.earth/term/belowGroundCropResidue)
  - [methodModel](https://hestia.earth/schema/Product#methodModel) with [pooreNemecek2018](https://hestia.earth/term/pooreNemecek2018)
  - [value](https://hestia.earth/schema/Product#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `False`

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `Default_bg_dm_crop_residue`
- [cropResidue.csv](https://hestia.earth/glossary/lookups/cropResidue.csv) -> `siteTypesAllowed`; `productTermTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.pooreNemecek2018 import run

print(run('belowGroundCropResidue', Cycle))
```
