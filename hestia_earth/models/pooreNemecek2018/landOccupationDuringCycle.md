## Land occupation, during Cycle

The amount of land required to produce the Product during the Cycle, multiplied by the time (in years) that the land was occupied including fallow periods.

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [landOccupationDuringCycle](https://hestia.earth/term/landOccupationDuringCycle)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [pooreNemecek2018](https://hestia.earth/term/pooreNemecek2018)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `agri-food processor` **or** `food retailer`
  - a [product](https://hestia.earth/schema/ImpactAssessment#product)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - either:
      - if the [cycle.functionalUnit](https://hestia.earth/schema/Cycle#functionalUnit) = 1 ha, additional properties are required:
        - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)
        - a list of [products](https://hestia.earth/schema/Cycle#products) with:
          - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0`
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [longFallowRatio](https://hestia.earth/term/longFallowRatio)
      - for plantations, additional properties are required:
        - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [nurseryDensity](https://hestia.earth/term/nurseryDensity)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [nurseryDuration](https://hestia.earth/term/nurseryDuration)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [plantationProductiveLifespan](https://hestia.earth/term/plantationProductiveLifespan)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [plantationDensity](https://hestia.earth/term/plantationDensity)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [plantationLifespan](https://hestia.earth/term/plantationLifespan)
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [rotationDuration](https://hestia.earth/term/rotationDuration)

### Lookup used

- [resourceUse.csv](https://hestia.earth/glossary/lookups/resourceUse.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.pooreNemecek2018 import run

print(run('landOccupationDuringCycle', ImpactAssessment))
```
