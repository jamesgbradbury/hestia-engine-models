# Poore Nemecek (2018)

These models implement the gap filling, emissions, and resource use models described in the supporting material of [Poore & Nemecek (2018)](https://doi.org/10.1126/science.aaq0216).
