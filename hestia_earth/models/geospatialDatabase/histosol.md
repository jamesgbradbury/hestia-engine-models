## Histosol

Soils having organic material: 1. starting at the soil surface and having a thickness of ≥ 10 cm and directly overlying: a. ice, or b. continuous rock or technic hard material, or c. coarse fragments, the interstices of which are filled with organic material; or 2. starting ≤ 40 cm from the soil surface and having within ≤ 100 cm of the soil surface a combined thickness of either: a. ≥ 60 cm, if ≥ 75% (by volume) of the material consists of moss fibres; or b. ≥ 40 cm in other materials. The area of the Site occupied by histosols can be specified as a percentage.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [histosol](https://hestia.earth/term/histosol)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `geospatial dataset`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - either:
    - the following fields:
      - [latitude](https://hestia.earth/schema/Site#latitude)
      - [longitude](https://hestia.earth/schema/Site#longitude)
    - the following fields:
      - a [boundary](https://hestia.earth/schema/Site#boundary)
    - the following fields:
      - a [region](https://hestia.earth/schema/Site#region) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - none of:
    - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) of [termType](https://hestia.earth/schema/Term#termType) = [soilType](https://hestia.earth/glossary?termType=soilType)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.geospatialDatabase import run

print(run('histosol', Site))
```
