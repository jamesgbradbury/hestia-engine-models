## Region

This model finds the region that contains the coordinates provided.

### Returns

* A [Term](https://hestia.earth/schema/Term) with:
  - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - [latitude](https://hestia.earth/schema/Site#latitude)
  - [longitude](https://hestia.earth/schema/Site#longitude)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.geospatialDatabase import run

print(run('region', Site))
```
