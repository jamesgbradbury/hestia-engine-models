## Land transformation, from temporary cropland, 20 year average, during Cycle

The amount of land used by this Cycle, that changed use from temporary cropland to the current use in the last 20 years, divided by 20.

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [landTransformationFromTemporaryCropland20YearAverageDuringCycle](https://hestia.earth/term/landTransformationFromTemporaryCropland20YearAverageDuringCycle) **or** [landTransformationFromPermanentCropland20YearAverageDuringCycle](https://hestia.earth/term/landTransformationFromPermanentCropland20YearAverageDuringCycle)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [faostat2018](https://hestia.earth/term/faostat2018)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `glass or high accessible cover` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `agri-food processor` **or** `food retailer`
  - [endDate](https://hestia.earth/schema/ImpactAssessment#endDate)
  - a [country](https://hestia.earth/schema/ImpactAssessment#country) with:
    - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) with [landTransformationFromCropland20YearAverageDuringCycle](https://hestia.earth/term/landTransformationFromCropland20YearAverageDuringCycle)

### Lookup used

- [region-faostatCroplandArea.csv](https://hestia.earth/glossary/lookups/region-faostatCroplandArea.csv)
- [resourceUse.csv](https://hestia.earth/glossary/lookups/resourceUse.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.faostat2018 import run

print(run('landTransformationFromCropland20YearAverage', ImpactAssessment))
```
