# CML2001 Non-Baseline

These models characterise emissions and resource uses according to the CML2001 Non-Baseline method, see [Guinée et al (2002)](https://www.universiteitleiden.nl/binaries/content/assets/science/cml/publicaties_pdf/new-dutch-lca-guide/part1.pdf).
