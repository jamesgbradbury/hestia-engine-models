## N2O, to air, natural vegetation burning, direct

Direct nitrous oxide emissions to air, from natural vegetation burning during deforestation or other land conversion.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [n2OToAirNaturalVegetationBurningDirect](https://hestia.earth/term/n2OToAirNaturalVegetationBurningDirect)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [blonkConsultants2016](https://hestia.earth/term/blonkConsultants2016)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` and a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - either:
    - if the [cycle.functionalUnit](https://hestia.earth/schema/Cycle#functionalUnit) = 1 ha, additional properties are required:
      - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)
      - a list of [products](https://hestia.earth/schema/Cycle#products) with:
        - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0`
      - a [site](https://hestia.earth/schema/Cycle#site) with:
        - a list of [practices](https://hestia.earth/schema/Site#practices) with:
          - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [longFallowRatio](https://hestia.earth/term/longFallowRatio)
    - for plantations, additional properties are required:
      - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
        - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [nurseryDensity](https://hestia.earth/term/nurseryDensity)
        - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [nurseryDuration](https://hestia.earth/term/nurseryDuration)
        - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [plantationProductiveLifespan](https://hestia.earth/term/plantationProductiveLifespan)
        - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [plantationDensity](https://hestia.earth/term/plantationDensity)
        - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [plantationLifespan](https://hestia.earth/term/plantationLifespan)
        - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) with [rotationDuration](https://hestia.earth/term/rotationDuration)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `isPlantation`; `cropGroupingFaostatArea`
- [region-crop-cropGroupingFaostatArea-n2oforestBiomassBurning.csv](https://hestia.earth/glossary/lookups/region-crop-cropGroupingFaostatArea-n2oforestBiomassBurning.csv) -> use crop grouping above or default to site.siteType
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.blonkConsultants2016 import run

print(run('n2OToAirNaturalVegetationBurningDirect', Cycle))
```
