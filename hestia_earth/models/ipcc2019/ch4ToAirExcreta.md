## CH4, to air, excreta

Methane emissions to air, from animal excreta.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [ch4ToAirExcreta](https://hestia.earth/term/ch4ToAirExcreta)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [ipcc2019](https://hestia.earth/term/ipcc2019)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 2`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `permanent pasture` **or** `animal housing` **or** `pond` **or** `river or stream` **or** `lake` **or** `sea or ocean` and a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region) and a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta)
  - Data completeness assessment for excreta: [completeness.excreta](https://hestia.earth/schema/Completeness#excreta)
  - [cycleDuration](https://hestia.earth/schema/Cycle#cycleDuration)
  - [endDate](https://hestia.earth/schema/Cycle#endDate)
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [value](https://hestia.earth/schema/Practice#value) and [term](https://hestia.earth/schema/Practice#term) of [termType](https://hestia.earth/schema/Term#termType) = [excretaManagement](https://hestia.earth/glossary?termType=excretaManagement)

This model works on the following Node type with identical requirements:

* [Cycle](https://hestia.earth/schema/Cycle)
* [Transformation](https://hestia.earth/schema/Transformation)

### Lookup used

- [region.csv](https://hestia.earth/glossary/lookups/region.csv) -> `HDI`
- [region-excreta-excretaManagement-ch4B0.csv](https://hestia.earth/glossary/lookups/region-excreta-excretaManagement-ch4B0.csv) -> use input `@id`
- [excretaManagement-ecoClimateZone-CH4conv.csv](https://hestia.earth/glossary/lookups/excretaManagement-ecoClimateZone-CH4conv.csv) -> use `ecoClimateZone` from site measurements
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `inputTermTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2019 import run

print(run('ch4ToAirExcreta', Cycle))
```
