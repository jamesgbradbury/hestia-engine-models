# Webb et al (2012) and Sintermann et al (2012)

This model calculates the NH3 emissions due to the addition of organic fertiliser based on a compilation of emissions factors from [Webb et al (2012)](https://doi.org/10.1007/978-94-007-1905-7_4) and [Sintermann et al (2012)](https://doi.org/10.5194/bg-9-1611-2012). The methodology for compiling these emissions is detailed in [Poore & Nemecek (2018)](https://doi.org/10.1126/science.aaq0216).
