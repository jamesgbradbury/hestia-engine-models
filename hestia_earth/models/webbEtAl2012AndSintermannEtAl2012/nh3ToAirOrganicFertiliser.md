## NH3, to air, organic fertiliser

Ammonia emissions to air, from organic fertiliser volatilization.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [nh3ToAirOrganicFertiliser](https://hestia.earth/term/nh3ToAirOrganicFertiliser)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [webbEtAl2012AndSintermannEtAl2012](https://hestia.earth/term/webbEtAl2012AndSintermannEtAl2012)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - Data completeness assessment for fertiliser: [completeness.fertiliser](https://hestia.earth/schema/Completeness#fertiliser)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [organicFertiliser](https://hestia.earth/glossary?termType=organicFertiliser) and a list of [properties](https://hestia.earth/schema/Input#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [totalAmmoniacalNitrogenContentAsN](https://hestia.earth/term/totalAmmoniacalNitrogenContentAsN)

### Lookup used

- [organicFertiliser.csv](https://hestia.earth/glossary/lookups/organicFertiliser.csv) -> `OrganicFertiliserClassification`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.webbEtAl2012AndSintermannEtAl2012 import run

print(run('nh3ToAirOrganicFertiliser', Cycle))
```
