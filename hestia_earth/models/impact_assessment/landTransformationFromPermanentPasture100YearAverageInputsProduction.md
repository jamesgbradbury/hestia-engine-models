## Land transformation, from permanent pasture, 100 year average, inputs production

The amount of land used to produce the inputs used by this Cycle, that changed use from permanent pasture to the current use in the last 100 years, divided by 100.

### Returns

* A list of [Indicators](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [landTransformationFromPermanentPasture100YearAverageInputsProduction](https://hestia.earth/term/landTransformationFromPermanentPasture100YearAverageInputsProduction)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` **or** `animal housing` **or** `pond` **or** `agri-food processor` **or** `food retailer`
  - a [product](https://hestia.earth/schema/ImpactAssessment#product)
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [primary](https://hestia.earth/schema/Product#primary) = `True` and [value](https://hestia.earth/schema/Product#value) `> 0` and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) `> 0` and a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - a [impactAssessment](https://hestia.earth/schema/Input#impactAssessment) with:
        - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
          - [term](https://hestia.earth/schema/Indicator#term) with [landTransformationFromPermanentPasture100YearAverageDuringCycle](https://hestia.earth/term/landTransformationFromPermanentPasture100YearAverageDuringCycle) and [value](https://hestia.earth/schema/Indicator#value) `> 0`

### Lookup used

- [resourceUse.csv](https://hestia.earth/glossary/lookups/resourceUse.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.impact_assessment import run

print(run('landTransformationFromPermanentPasture100YearAverageInputsProduction', ImpactAssessment))
```
