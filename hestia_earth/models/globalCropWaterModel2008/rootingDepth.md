## Rooting depth

The rooting depth of the crop.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - a list of [properties](https://hestia.earth/schema/Product#properties) with:
    - [term](https://hestia.earth/schema/Property#term) with [rootingDepth](https://hestia.earth/term/rootingDepth)
    - [methodModel](https://hestia.earth/schema/Property#methodModel) with [globalCropWaterModel2008](https://hestia.earth/term/globalCropWaterModel2008)
    - [value](https://hestia.earth/schema/Property#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - [functionalUnit](https://hestia.earth/schema/Cycle#functionalUnit) with `1 ha`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop)
  - optional:
    - Data completeness assessment for water: [completeness.water](https://hestia.earth/schema/Completeness#water)
    - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [water](https://hestia.earth/glossary?termType=water)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `Rooting_depth_irrigated_m`; `Rooting_depth_rainfed_m`; `Rooting_depth_average_m`
- [property.csv](https://hestia.earth/glossary/lookups/property.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.globalCropWaterModel2008 import run

print(run('rootingDepth', Cycle))
```
