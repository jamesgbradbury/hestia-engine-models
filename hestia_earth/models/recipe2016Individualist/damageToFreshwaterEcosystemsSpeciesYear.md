## Damage to freshwater ecosystems (species*year)

The number of freshwater species that are committed to local extinction over a certain period of time if the pressure continues to happen. See [ReCiPe 2016](https://pre-sustainability.com/legacy/download/Report_ReCiPe_2017.pdf).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToFreshwaterEcosystemsSpeciesYear](https://hestia.earth/term/damageToFreshwaterEcosystemsSpeciesYear)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Individualist](https://hestia.earth/term/recipe2016Individualist)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [impacts](https://hestia.earth/schema/ImpactAssessment#impacts) with:
    - [value](https://hestia.earth/schema/Indicator#value) and a [methodModel](https://hestia.earth/schema/Indicator#methodModel) with:
      - [@id](https://hestia.earth/schema/Term#id) must be set (is linked to an existing Term)

### Lookup used

- [characterisedIndicator.csv](https://hestia.earth/glossary/lookups/characterisedIndicator.csv) -> `speciesYearIndividualistDamageToFreshwaterEcosystemsReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Individualist import run

print(run('damageToFreshwaterEcosystemsSpeciesYear', ImpactAssessment))
```
