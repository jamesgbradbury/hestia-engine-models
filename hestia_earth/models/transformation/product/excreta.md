## Product Excreta

This model calculates the value of every `Product` by taking the value of the `Input` with the same `term`.
It also adds other variants of the same `Product` in different `units` (e.g. `kg N`, `kg VS` and `kg`).
Note: this model also substract `Emissions` for `Input` with `units` = `kg N`.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta)
  - [value](https://hestia.earth/schema/Product#value)

### Requirements

* A [Transformation](https://hestia.earth/schema/Transformation) with:
  - [term](https://hestia.earth/schema/Transformation#term) of [termType](https://hestia.earth/schema/Term#termType) = [excretaManagement](https://hestia.earth/glossary?termType=excretaManagement)
  - a list of [inputs](https://hestia.earth/schema/Transformation#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta)
  - a list of [products](https://hestia.earth/schema/Transformation#products) with:
    - [value](https://hestia.earth/schema/Product#value)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `causesExcretaMassLoss`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.transformation import run

print(run('product.excreta', Transformation))
```
