## Input Value

This model calculates the [Input min](https://hestia.earth/schema/Input#min)
by taking the min of the same [Product](https://hestia.earth/schema/Product) of the
previous Transformation (or Cycle if first Transformation) and applying the
[share](https://hestia.earth/schema/Transformation/transformedShare).

### Returns

* A list of [Transformations](https://hestia.earth/schema/Transformation) with:
  - a list of [inputs](https://hestia.earth/schema/Transformation#inputs) with:
    - [min](https://hestia.earth/schema/Input#min)
    - [statsDefinition](https://hestia.earth/schema/Input#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [min](https://hestia.earth/schema/Product#min)
  - a list of [transformations](https://hestia.earth/schema/Cycle#transformations) with:
    - [transformedShare](https://hestia.earth/schema/Transformation#transformedShare) and a list of [inputs](https://hestia.earth/schema/Transformation#inputs)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.transformation import run

print(run('input.min', Cycle))
```
