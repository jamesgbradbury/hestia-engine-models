## Input Properties

This model copies the [Product properties](https://hestia.earth/schema/Product#properties) of the Cycle
to the first transformations [inputs](https://hestia.earth/schema/Transformation#inputs).

### Returns

* A list of [Transformations](https://hestia.earth/schema/Transformation) with:
  - a list of [inputs](https://hestia.earth/schema/Transformation#inputs)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value)
  - a list of [transformations](https://hestia.earth/schema/Cycle#transformations) with:
    - a list of [inputs](https://hestia.earth/schema/Transformation#inputs) and none of:
      - [previousTransformationId](https://hestia.earth/schema/Transformation#previousTransformationId)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.transformation import run

print(run('input.properties', Cycle))
```
