# Koble (2014)

This model estimates the amount of crop residue burnt and removed using country average factors for different crop groupings. The data are described in [The Global Nitrous Oxide Calculator – GNOC ](https://op.europa.eu/en/publication-detail/-/publication/bd666ed9-ec34-4d9d-90d1-6f5608052b1f/language-en).
