## Above Ground Crop Residue

This model returns the amounts and destinations of above ground crop residue, working in the following order:
1. [Above ground crop residue, removed](https://hestia.earth/term/aboveGroundCropResidueRemoved);
2. [Above ground crop residue, incorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated);
3. [Above ground crop residue, burnt](https://hestia.earth/term/aboveGroundCropResidueBurnt);
4. [Above ground crop residue, left on field](https://hestia.earth/term/aboveGroundCropResidueLeftOnField).

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) **or** [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) **or** [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) **or** [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)
  - [methodModel](https://hestia.earth/schema/Product#methodModel) with [koble2014](https://hestia.earth/term/koble2014)
  - [value](https://hestia.earth/schema/Product#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `False`
  - a list of [practices](https://hestia.earth/schema/Cycle#practices) with:
    - [term](https://hestia.earth/schema/Practice#term) with [residueRemoved](https://hestia.earth/term/residueRemoved) **or** [residueIncorporated](https://hestia.earth/term/residueIncorporated) **or** [residueIncorporatedLessThan30DaysBeforeCultivation](https://hestia.earth/term/residueIncorporatedLessThan30DaysBeforeCultivation) **or** [residueIncorporatedMoreThan30DaysBeforeCultivation](https://hestia.earth/term/residueIncorporatedMoreThan30DaysBeforeCultivation) **or** [residueBurnt](https://hestia.earth/term/residueBurnt)

### Lookup used

- [cropResidue.csv](https://hestia.earth/glossary/lookups/cropResidue.csv) -> `siteTypesAllowed`; `productTermTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.koble2014 import run

print(run('aboveGroundCropResidue', Cycle))
```
