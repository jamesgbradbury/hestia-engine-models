# HYDE 3.2

This model uses the [HYDE database v3.2](https://public.yoda.uu.nl/geo/UU01/MO2FF3.html) to calculate land use and land use change.
