## Freshwater ecotoxicity potential (CTUe)

The potential of chemicals to cause toxic effects in freshwater ecosystems, expressed as an estimate of the potentially affected fraction of species (PAF) integrated over time and volume. This unit is also referred to as Comparative Toxic Unit for ecosystems (CTUe).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [freshwaterEcotoxicityPotentialCtue](https://hestia.earth/term/freshwaterEcotoxicityPotentialCtue)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [environmentalFootprintV3](https://hestia.earth/term/environmentalFootprintV3)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - Data completeness assessment for pesticidesAntibiotics: [completeness.pesticidesAntibiotics](https://hestia.earth/schema/Completeness#pesticidesAntibiotics) must be `True` and a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [pesticideAI](https://hestia.earth/glossary?termType=pesticideAI)

### Lookup used

- [pesticideAI.csv](https://hestia.earth/glossary/lookups/pesticideAI.csv) -> `pafM3DFreshwaterEcotoxicityUsetox2-1Hc20Ec10eq`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.environmentalFootprintV3 import run

print(run('freshwaterEcotoxicityPotentialCtue', ImpactAssessment))
```
