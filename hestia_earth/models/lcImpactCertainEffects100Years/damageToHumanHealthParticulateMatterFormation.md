## Damage to human health, particulate matter formation

The disability-adjusted life years lost in the human population due to particulate matter formation. See [lc-impact.eu](https://lc-impact.eu/HHparticular_matter_formation.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToHumanHealthParticulateMatterFormation](https://hestia.earth/term/damageToHumanHealthParticulateMatterFormation)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [lcImpactCertainEffects100Years](https://hestia.earth/term/lcImpactCertainEffects100Years)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

- [region-emission-ParticulateMatterFormationCertainEffectsDamageToHumanHealthLCImpactCF.csv](https://hestia.earth/glossary/lookups/region-emission-ParticulateMatterFormationCertainEffectsDamageToHumanHealthLCImpactCF.csv)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.lcImpactCertainEffects100Years import run

print(run('damageToHumanHealthParticulateMatterFormation', ImpactAssessment))
```
