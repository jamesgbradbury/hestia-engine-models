## NOx, to air, inorganic fertiliser

Nitrogen oxides emissions to air, from inorganic fertiliser nitrification and denitrification.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [noxToAirInorganicFertiliser](https://hestia.earth/term/noxToAirInorganicFertiliser)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [stehfestBouwman2006GisImplementation](https://hestia.earth/term/stehfestBouwman2006GisImplementation)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture` and a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)
  - Data completeness assessment for products: [completeness.products](https://hestia.earth/schema/Completeness#products)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `True`
  - Data completeness assessment for fertiliser: [completeness.fertiliser](https://hestia.earth/schema/Completeness#fertiliser)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [cropResidue](https://hestia.earth/glossary?termType=cropResidue) **or** [excreta](https://hestia.earth/glossary?termType=excreta) and a list of [properties](https://hestia.earth/schema/Product#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [units](https://hestia.earth/schema/Term#units) = `kg` **or** `kg N` and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [organicFertiliser](https://hestia.earth/glossary?termType=organicFertiliser) **or** [inorganicFertiliser](https://hestia.earth/glossary?termType=inorganicFertiliser) **or** [excreta](https://hestia.earth/glossary?termType=excreta) and optional:
      - a list of [properties](https://hestia.earth/schema/Input#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)

### Lookup used

- [region.csv](https://hestia.earth/glossary/lookups/region.csv) -> `EF_NOX`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.stehfestBouwman2006GisImplementation import run

print(run('noxToAirInorganicFertiliser', Cycle))
```
