# Stehfest Bouwman (2006) GIS Implementation

This model calculates the direct N2O and NOx emissions due to the use of fertiliser, by creating a country-average version of the [Stehfest & Bouwman (2006)](https://doi.org/10.1007/s10705-006-9000-7) model using GIS software.
