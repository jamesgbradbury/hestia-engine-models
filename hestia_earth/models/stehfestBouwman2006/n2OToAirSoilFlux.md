## N2O, to air, soil flux

The total amount of nitrous oxide emissions to air, from the soil, including from nitrogen added in fertiliser, excreta, and residue, and from natural background processes.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [n2OToAirSoilFlux](https://hestia.earth/term/n2OToAirSoilFlux)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [stehfestBouwman2006](https://hestia.earth/term/stehfestBouwman2006)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 2`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - Data completeness assessment for products: [completeness.products](https://hestia.earth/schema/Completeness#products)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `True`
  - Data completeness assessment for fertiliser: [completeness.fertiliser](https://hestia.earth/schema/Completeness#fertiliser)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [cropResidue](https://hestia.earth/glossary?termType=cropResidue) **or** [excreta](https://hestia.earth/glossary?termType=excreta) and a list of [properties](https://hestia.earth/schema/Product#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [units](https://hestia.earth/schema/Term#units) = `kg` **or** `kg N` and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [organicFertiliser](https://hestia.earth/glossary?termType=organicFertiliser) **or** [inorganicFertiliser](https://hestia.earth/glossary?termType=inorganicFertiliser) **or** [excreta](https://hestia.earth/glossary?termType=excreta) and optional:
      - a list of [properties](https://hestia.earth/schema/Input#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [nitrogenContent](https://hestia.earth/term/nitrogenContent)
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [totalNitrogenPerKgSoil](https://hestia.earth/term/totalNitrogenPerKgSoil)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [organicCarbonPerKgSoil](https://hestia.earth/term/organicCarbonPerKgSoil)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [clayContent](https://hestia.earth/term/clayContent)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [sandContent](https://hestia.earth/term/sandContent)
      - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [soilPh](https://hestia.earth/term/soilPh)

### Lookup used

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `cropGroupingStehfestBouwman`
- [ecoClimateZone.csv](https://hestia.earth/glossary/lookups/ecoClimateZone.csv) -> `STEHFEST_BOUWMAN_2006_N2O-N_FACTOR`
- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`
- [emission-model-siteTypesAllowed.csv](https://hestia.earth/glossary/lookups/emission-model-siteTypesAllowed.csv)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.stehfestBouwman2006 import run

print(run('n2OToAirSoilFlux', Cycle))
```
