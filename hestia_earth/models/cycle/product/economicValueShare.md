## Product Economic Value Share

This model quantifies the relative economic value share of each marketable Product in a Cycle.
Marketable Products are all Products in the Glossary with the exception of crop residue not sold.

It works in the following order:
1. If revenue data are provided for all marketable products,
the `economicValueShare` is directly calculated as the share of revenue of each Product;
2. If the primary product is a crop and it is the only crop Product,
`economicValueShare` is assigned based on a lookup table containing typical global average economic value shares
drawn from [Poore & Nemecek (2018)](https://science.sciencemag.org/content/360/6392/987).

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [economicValueShare](https://hestia.earth/schema/Product#economicValueShare)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [economicValueShare](https://hestia.earth/schema/Product#economicValueShare) and [revenue](https://hestia.earth/schema/Product#revenue) and [currency](https://hestia.earth/schema/Product#currency)
  - optional:
    - Data completeness assessment for products: [completeness.products](https://hestia.earth/schema/Completeness#products)

### Lookup used

Depending on the primary product [termType](https://hestia.earth/schema/Product#term):

- [crop.csv](https://hestia.earth/glossary/lookups/crop.csv) -> `global_economic_value_share`
- [excreta.csv](https://hestia.earth/glossary/lookups/excreta.csv) -> `global_economic_value_share`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('product.economicValueShare', Cycle))
```
