## Product Primary

Determines the primary product which is the product with the highest `economicValueShare`.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [primary](https://hestia.earth/schema/Product#primary) = `True`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [economicValueShare](https://hestia.earth/schema/Product#economicValueShare)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('product.primary', Cycle))
```
