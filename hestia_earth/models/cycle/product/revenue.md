## Product Revenue

This model calculates the revenue of each product by multiplying the yield with the revenue.

In the case the product `value` is `0`, the `revenue` will be set to `0`.

In the case the product `price` is `0`, the `revenue` will be set to `0`.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [revenue](https://hestia.earth/schema/Product#revenue)
  - [currency](https://hestia.earth/schema/Product#currency) with `defaults to USD if multiple currencies are used`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [price](https://hestia.earth/schema/Product#price) and optional:
      - [value](https://hestia.earth/schema/Product#value)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('product.revenue', Cycle))
```
