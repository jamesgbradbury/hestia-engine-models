## Feed conversion ratio (nitrogen)

The feed conversion ratio (kg N of feed per kg of liveweight produced), based on the nitrogen content of the feed.

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [term](https://hestia.earth/schema/Practice#term) with [feedConversionRatioNitrogen](https://hestia.earth/term/feedConversionRatioNitrogen)
  - [value](https://hestia.earth/schema/Practice#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `permanent pasture` **or** `animal housing`
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [term](https://hestia.earth/schema/Input#term) of [units](https://hestia.earth/schema/Term#units) = `kg` and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [animalProduct](https://hestia.earth/glossary?termType=animalProduct) **or** [feedFoodAdditive](https://hestia.earth/glossary?termType=feedFoodAdditive) and [value](https://hestia.earth/schema/Input#value) `> 0` and [isAnimalFeed](https://hestia.earth/schema/Input#isAnimalFeed) with `True` and a list of [properties](https://hestia.earth/schema/Input#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [energyContentHigherHeatingValue](https://hestia.earth/term/energyContentHigherHeatingValue)
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [crudeProteinContent](https://hestia.earth/term/crudeProteinContent)
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [animalProduct](https://hestia.earth/glossary?termType=animalProduct) and optional:
      - a list of [properties](https://hestia.earth/schema/Product#properties) with:
        - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [processingConversionLiveweightToColdCarcassWeight](https://hestia.earth/term/processingConversionLiveweightToColdCarcassWeight) **or** [processingConversionLiveweightToColdDressedCarcassWeight](https://hestia.earth/term/processingConversionLiveweightToColdDressedCarcassWeight) **or** [processingConversionColdCarcassWeightToReadyToCookWeight](https://hestia.earth/term/processingConversionColdCarcassWeightToReadyToCookWeight) **or** [processingConversionColdDressedCarcassWeightToReadyToCookWeight](https://hestia.earth/term/processingConversionColdDressedCarcassWeightToReadyToCookWeight)

### Lookup used

- [crop-property.csv](https://hestia.earth/glossary/lookups/crop-property.csv) -> `crudeProteinContent`
- [animalManagement.csv](https://hestia.earth/glossary/lookups/animalManagement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('feedConversionRatio.feedConversionRatioNitrogen', Cycle))
```
