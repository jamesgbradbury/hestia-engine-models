## Input Properties

This model adds properties to the `Input` when they are connected to another `Cycle` via the
[impactAssessment](https://hestia.earth/schema/Input#impactAssessment) field.

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - a list of [properties](https://hestia.earth/schema/Input#properties)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [impactAssessment](https://hestia.earth/schema/Input#impactAssessment)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('input.properties', Cycle))
```
