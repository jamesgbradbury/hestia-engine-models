## Residue burnt

The share of above ground crop residue burnt, after multiplication by the combustion factor (which allows for residue burnt but not combusted).

### Returns

* A list of [Practices](https://hestia.earth/schema/Practice) with:
  - [term](https://hestia.earth/schema/Practice#term) with [residueBurnt](https://hestia.earth/term/residueBurnt)
  - [value](https://hestia.earth/schema/Practice#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) and [value](https://hestia.earth/schema/Product#value) `> 0`
    - [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) and [value](https://hestia.earth/schema/Product#value) `> 0`
    - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [crop](https://hestia.earth/glossary?termType=crop) **or** [forage](https://hestia.earth/glossary?termType=forage)
  - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `False`

### Lookup used

- [cropResidueManagement.csv](https://hestia.earth/glossary/lookups/cropResidueManagement.csv) -> `siteTypesAllowed`; `productTermTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('residueBurnt', Cycle))
```
