## Inorganic Fertiliser

This model calculates the amount of other nutrient(s) supplied by multi-nutrients inorganic fertilisers when only
the amount of one of the nutrients is recorded by the user.

### Returns

* A list of [Inputs](https://hestia.earth/schema/Input) with:
  - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [inorganicFertiliser](https://hestia.earth/glossary?termType=inorganicFertiliser)
  - [value](https://hestia.earth/schema/Input#value)
  - [min](https://hestia.earth/schema/Input#min)
  - [max](https://hestia.earth/schema/Input#max)
  - [statsDefinition](https://hestia.earth/schema/Input#statsDefinition) with `modelled`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
    - [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [inorganicFertiliser](https://hestia.earth/glossary?termType=inorganicFertiliser) and [value](https://hestia.earth/schema/Input#value) `> 0`

### Lookup used

- [inorganicFertiliser.csv](https://hestia.earth/glossary/lookups/inorganicFertiliser.csv) -> `mustIncludeId`; `nitrogenContent`; `nitrogenContent-min`; `nitrogenContent-max`; `phosphateContentAsP2O5`; `phosphateContentAsP2O5-min`; `phosphateContentAsP2O5-max`; `potassiumContentAsK2O`; `potassiumContentAsK2O-min`; `potassiumContentAsK2O-max`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('inorganicFertiliser', Cycle))
```
