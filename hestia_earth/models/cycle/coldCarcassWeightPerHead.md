## Cold carcass weight per head

The average cold carcass weight of the animals per head.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - a list of [properties](https://hestia.earth/schema/Product#properties) with:
    - [term](https://hestia.earth/schema/Property#term) with [coldCarcassWeightPerHead](https://hestia.earth/term/coldCarcassWeightPerHead)
    - [value](https://hestia.earth/schema/Property#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [value](https://hestia.earth/schema/Product#value) and [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [animalProduct](https://hestia.earth/glossary?termType=animalProduct) and [term](https://hestia.earth/schema/Product#term) of [units](https://hestia.earth/schema/Term#units) = `kg cold carcass weight` and a list of [properties](https://hestia.earth/schema/Product#properties) with:
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [liveweightPerHead](https://hestia.earth/term/liveweightPerHead)
      - [value](https://hestia.earth/schema/Property#value) and [term](https://hestia.earth/schema/Property#term) with [processingConversionLiveweightToColdCarcassWeight](https://hestia.earth/term/processingConversionLiveweightToColdCarcassWeight)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('coldCarcassWeightPerHead', Cycle))
```
