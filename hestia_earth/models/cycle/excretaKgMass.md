## Excreta (kg)

This model calculates the amount of excreta in `kg` based on the amount of excreta in `kg N` or `kg Vs`.

### Returns

* A list of [Products](https://hestia.earth/schema/Product) with:
  - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta)
  - [term](https://hestia.earth/schema/Product#term) of [units](https://hestia.earth/schema/Term#units) = `kg`
  - [value](https://hestia.earth/schema/Product#value)

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a list of [products](https://hestia.earth/schema/Cycle#products) with:
    - [term](https://hestia.earth/schema/Product#term) of [termType](https://hestia.earth/schema/Term#termType) = [excreta](https://hestia.earth/glossary?termType=excreta) and [term](https://hestia.earth/schema/Product#term) of [units](https://hestia.earth/schema/Term#units) = `kg N` **or** `kg VS`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.cycle import run

print(run('excretaKgMass', Cycle))
```
