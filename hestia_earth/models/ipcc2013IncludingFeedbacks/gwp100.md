## GWP100

The global warming potential of mixed greenhouse gases on the mid-term climate (100 years), expressed as CO2 equivalents.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [gwp100](https://hestia.earth/term/gwp100)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [ipcc2013IncludingFeedbacks](https://hestia.earth/term/ipcc2013IncludingFeedbacks)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `co2EqGwp100IncludingClimate-CarbonFeedbacksIpcc2013`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.ipcc2013IncludingFeedbacks import run

print(run('gwp100', ImpactAssessment))
```
