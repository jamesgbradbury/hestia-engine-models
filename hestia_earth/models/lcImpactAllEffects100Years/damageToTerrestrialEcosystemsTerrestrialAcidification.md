## Damage to terrestrial ecosystems, terrestrial acidification

The fraction of species richness that may be potentially lost in terrestrial ecosystems due to terrestrial acidification. See [lc-impact.eu](https://lc-impact.eu/EQacidification.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToTerrestrialEcosystemsTerrestrialAcidification](https://hestia.earth/term/damageToTerrestrialEcosystemsTerrestrialAcidification)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [lcImpactAllEffects100Years](https://hestia.earth/term/lcImpactAllEffects100Years)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

- [region-emission-TerrestrialAcidificationDamageToTerrestrialEcosystemsLCImpactCF.csv](https://hestia.earth/glossary/lookups/region-emission-TerrestrialAcidificationDamageToTerrestrialEcosystemsLCImpactCF.csv)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.lcImpactAllEffects100Years import run

print(run('damageToTerrestrialEcosystemsTerrestrialAcidification', ImpactAssessment))
```
