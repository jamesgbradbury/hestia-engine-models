## Damage to freshwater ecosystems, water stress

The fraction of species richness that may be potentially lost in freshwater ecosystems due to water stress. See [lc-impact.eu](https://lc-impact.eu/EQwater_stress.html).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToFreshwaterEcosystemsWaterStress](https://hestia.earth/term/damageToFreshwaterEcosystemsWaterStress)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [lcImpactAllEffects100Years](https://hestia.earth/term/lcImpactAllEffects100Years)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [resourceUse](https://hestia.earth/glossary?termType=resourceUse)
  - a [site](https://hestia.earth/schema/ImpactAssessment#site) with:
    - a [country](https://hestia.earth/schema/Site#country) with:
      - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Lookup used

- [region-resourceUse-WaterStressDamageToFreshwaterEcosystemsLCImpactCF.csv](https://hestia.earth/glossary/lookups/region-resourceUse-WaterStressDamageToFreshwaterEcosystemsLCImpactCF.csv)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.lcImpactAllEffects100Years import run

print(run('damageToFreshwaterEcosystemsWaterStress', ImpactAssessment))
```
