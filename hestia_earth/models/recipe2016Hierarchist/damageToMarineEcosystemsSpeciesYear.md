## Damage to marine ecosystems (species*year)

The number of marine species that are committed to local extinction over a certain period of time if the pressure continues to happen. See [ReCiPe 2016](https://pre-sustainability.com/legacy/download/Report_ReCiPe_2017.pdf).

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToMarineEcosystemsSpeciesYear](https://hestia.earth/term/damageToMarineEcosystemsSpeciesYear)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Hierarchist](https://hestia.earth/term/recipe2016Hierarchist)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [impacts](https://hestia.earth/schema/ImpactAssessment#impacts) with:
    - [value](https://hestia.earth/schema/Indicator#value) and a [methodModel](https://hestia.earth/schema/Indicator#methodModel) with:
      - [@id](https://hestia.earth/schema/Term#id) must be set (is linked to an existing Term)

### Lookup used

- [characterisedIndicator.csv](https://hestia.earth/glossary/lookups/characterisedIndicator.csv) -> `speciesYearHierarchistDamageToMarineEcosystemsReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Hierarchist import run

print(run('damageToMarineEcosystemsSpeciesYear', ImpactAssessment))
```
