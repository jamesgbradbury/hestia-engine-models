## Fossil resource scarcity

This model calculates the fossil resource scarcity.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [fossilResourceScarcity](https://hestia.earth/term/fossilResourceScarcity)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Hierarchist](https://hestia.earth/term/recipe2016Hierarchist)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - Data completeness assessment for electricityFuel: [completeness.electricityFuel](https://hestia.earth/schema/Completeness#electricityFuel) must be `True` and a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [fuel](https://hestia.earth/glossary?termType=fuel)

### Lookup used

- [fuel.csv](https://hestia.earth/glossary/lookups/fuel.csv) -> `oilEqHierarchistFossilResourceScarcityReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Hierarchist import run

print(run('fossilResourceScarcity', ImpactAssessment))
```
