## Ozone depletion potential

The potential of emissions to cause thinning of the stratospheric ozone layer.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [ozoneDepletionPotential](https://hestia.earth/term/ozoneDepletionPotential)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Hierarchist](https://hestia.earth/term/recipe2016Hierarchist)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [emissionsResourceUses](https://hestia.earth/schema/ImpactAssessment#emissionsResourceUse) with:
    - [value](https://hestia.earth/schema/Indicator#value) and [term](https://hestia.earth/schema/Indicator#term) of [termType](https://hestia.earth/schema/Term#termType) = [emission](https://hestia.earth/glossary?termType=emission)

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `cfc11EqHierarchistStratosphericOzoneDepletionReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Hierarchist import run

print(run('ozoneDepletionPotential', ImpactAssessment))
```
