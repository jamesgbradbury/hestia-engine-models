## Organic matter (per kg soil)

The concentration of organic matter in the soil. The term refers to any material produced originally by living organisms (plant or animal) that is returned to the soil and goes through the decomposition process.

### Returns

* A list of [Measurements](https://hestia.earth/schema/Measurement) with:
  - [term](https://hestia.earth/schema/Measurement#term) with [organicMatterPerKgSoil](https://hestia.earth/term/organicMatterPerKgSoil)
  - [value](https://hestia.earth/schema/Measurement#value)
  - [min](https://hestia.earth/schema/Measurement#min)
  - [max](https://hestia.earth/schema/Measurement#max)
  - [statsDefinition](https://hestia.earth/schema/Measurement#statsDefinition) with `modelled`
  - [methodClassification](https://hestia.earth/schema/Measurement#methodClassification) with `modelled using other physical measurements`

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - a [siteType](https://hestia.earth/schema/Site#siteType) = `forest` **or** `other natural vegetation` **or** `cropland` **or** `glass or high accessible cover` **or** `permanent pasture`
  - a list of [measurements](https://hestia.earth/schema/Site#measurements) with:
    - [value](https://hestia.earth/schema/Measurement#value) and [term](https://hestia.earth/schema/Measurement#term) with [organicCarbonPerKgSoil](https://hestia.earth/term/organicCarbonPerKgSoil)

### Lookup used

- [measurement.csv](https://hestia.earth/glossary/lookups/measurement.csv) -> `siteTypesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run('organicMatterPerKgSoil', Site))
```
