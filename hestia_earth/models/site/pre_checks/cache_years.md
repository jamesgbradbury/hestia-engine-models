## Pre Checks Cache Years

This model caches the years of all Cycles related to this Site.

### Returns

* A [Site](https://hestia.earth/schema/Site)

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run(Site))
```
