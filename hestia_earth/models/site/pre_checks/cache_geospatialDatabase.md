## Pre Checks Cache Geospatial Database

This model caches results from Geospatial Database.

### Returns

* A [Site](https://hestia.earth/schema/Site)

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:
  - either:
    - the following fields:
      - [latitude](https://hestia.earth/schema/Site#latitude)
      - [longitude](https://hestia.earth/schema/Site#longitude)
    - the following fields:
      - a [boundary](https://hestia.earth/schema/Site#boundary)
    - the following fields:
      - a [region](https://hestia.earth/schema/Site#region) with:
        - [termType](https://hestia.earth/schema/Term#termType) = [region](https://hestia.earth/glossary?termType=region)

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run(Site))
```
