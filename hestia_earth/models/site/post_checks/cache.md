## Post Checks Cache

This model removes any cached data on the Site.

### Returns

* A [Site](https://hestia.earth/schema/Site)

### Requirements

* A [Site](https://hestia.earth/schema/Site) with:

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.site import run

print(run(Site))
```
