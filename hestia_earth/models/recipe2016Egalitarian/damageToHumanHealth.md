## Damage to human health

The disability-adjusted life years lost in the human population.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [damageToHumanHealth](https://hestia.earth/term/damageToHumanHealth)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Egalitarian](https://hestia.earth/term/recipe2016Egalitarian)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a list of [impacts](https://hestia.earth/schema/ImpactAssessment#impacts) with:
    - [value](https://hestia.earth/schema/Indicator#value) and a [methodModel](https://hestia.earth/schema/Indicator#methodModel) with:
      - [@id](https://hestia.earth/schema/Term#id) must be set (is linked to an existing Term)

### Lookup used

- [characterisedIndicator.csv](https://hestia.earth/glossary/lookups/characterisedIndicator.csv) -> `dalyEgalitarianDamageToHumanHealthReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Egalitarian import run

print(run('damageToHumanHealth', ImpactAssessment))
```
