## Human carcinogenic toxicity

The potential of emissions to contribute to the risk of increased incidence of cancer diseases.

### Returns

* A [Indicator](https://hestia.earth/schema/Indicator) with:
  - [term](https://hestia.earth/schema/Indicator#term) with [humanCarcinogenicToxicity](https://hestia.earth/term/humanCarcinogenicToxicity)
  - [methodModel](https://hestia.earth/schema/Indicator#methodModel) with [recipe2016Egalitarian](https://hestia.earth/term/recipe2016Egalitarian)
  - [value](https://hestia.earth/schema/Indicator#value)

### Requirements

* A [ImpactAssessment](https://hestia.earth/schema/ImpactAssessment) with:
  - a [cycle](https://hestia.earth/schema/ImpactAssessment#cycle) with:
    - Data completeness assessment for pesticidesAntibiotics: [completeness.pesticidesAntibiotics](https://hestia.earth/schema/Completeness#pesticidesAntibiotics) must be `True` and a list of [inputs](https://hestia.earth/schema/Cycle#inputs) with:
      - [value](https://hestia.earth/schema/Input#value) and [term](https://hestia.earth/schema/Input#term) of [termType](https://hestia.earth/schema/Term#termType) = [pesticideAI](https://hestia.earth/glossary?termType=pesticideAI)

### Lookup used

- [pesticideAI.csv](https://hestia.earth/glossary/lookups/pesticideAI.csv) -> `14DCBeqEgalitarianHumanCarcinogenicToxicityReCiPe2016`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.recipe2016Egalitarian import run

print(run('humanCarcinogenicToxicity', ImpactAssessment))
```
