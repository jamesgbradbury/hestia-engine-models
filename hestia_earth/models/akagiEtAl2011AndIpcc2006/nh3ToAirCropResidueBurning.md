## NH3, to air, crop residue burning

Ammonia emissions to air, from crop residue burning.

### Returns

* A list of [Emissions](https://hestia.earth/schema/Emission) with:
  - [term](https://hestia.earth/schema/Emission#term) with [nh3ToAirCropResidueBurning](https://hestia.earth/term/nh3ToAirCropResidueBurning)
  - [methodModel](https://hestia.earth/schema/Emission#methodModel) with [akagiEtAl2011AndIpcc2006](https://hestia.earth/term/akagiEtAl2011AndIpcc2006)
  - [value](https://hestia.earth/schema/Emission#value)
  - [methodTier](https://hestia.earth/schema/Emission#methodTier) with `tier 1`

### Requirements

* A [Cycle](https://hestia.earth/schema/Cycle) with:
  - a [site](https://hestia.earth/schema/Cycle#site) with:
    - [siteType](https://hestia.earth/schema/Site#siteType) with `cropland`
  - either:
    - a list of [products](https://hestia.earth/schema/Cycle#products) with:
      - [term](https://hestia.earth/schema/Product#term) with [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) **or** [discardedCropBurnt](https://hestia.earth/term/discardedCropBurnt) and [value](https://hestia.earth/schema/Product#value)
    - Data completeness assessment for cropResidue: [completeness.cropResidue](https://hestia.earth/schema/Completeness#cropResidue) must be `True`

### Lookup used

- [emission.csv](https://hestia.earth/glossary/lookups/emission.csv) -> `siteTypesAllowed`; `typesAllowed`

### Usage

1. Install the library: `pip install hestia_earth.models`
2. Import the library and run the model:

```python
from hestia_earth.models.akagiEtAl2011AndIpcc2006 import run

print(run('nh3ToAirCropResidueBurning', Cycle))
```
