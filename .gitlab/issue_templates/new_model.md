# New Model

## Description
<!-- Describe the model in simple words. -->

## Requirements
<!-- Describe the requirements for the model to run. Example: need emission A and B, input C and D, a site with measurement / spatial coordinates, etc. -->

## Output
<!-- Describe what the model should return. Example: returns a Measurement with value, sd and statsDefinition filled in. -->

## Examples
<!--
Give a complete example here.
Ideally it could be:

**Input**

```json
{
  "@type": "Cycle",
  "products": [
    {
      "@type": "Product",
      "term": {
        "@type": "Term",
        "@id": "aboveGroundCropResidueBurnt"
      },
      "value": [
        123
      ]
    }
  ]
}
```

**Expected Output**

```json
{
  "@type": "Emission",
  "term": {
    "@type": "Term",
    "@id": "ch4ToAirCropResidueBurning"
  },
  "value": [
    1.4566
  ]
}
```

**Formula (pseudo-code)**

```
return sum(product in products if product.term in ['aboveGroundCropResidueBurnt'])
```
-->

## Knowledge tasks

- [ ] Fill-in the `Description`
- [ ] Fill-in the `Requirements` (data dependencies)
- [ ] Fill-in the `Output` (Assertion Test)

## Dev Tasks

- [ ] Create a branch
- [ ] Add the code
- [ ] Add the documentation
- [ ] Add the unit tests
- [ ] Create a Merge Request

/label ~feature
