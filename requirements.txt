hestia_earth.schema>=23.7.0
hestia_earth.utils>=0.11.12
python-dateutil>=2.8.1
CurrencyConverter==0.16.8
haversine>=2.7.0
pydash
